import os
from typing import Any, Tuple

import pytest

from pytest_gluetool import CitoolRunnerType
from pytest_gluetool.scenario import Scenario, load_scenarios, run_scenario


def _run_scenario(
    citool: Tuple[CitoolRunnerType, str], variables: dict[str, Any], scenario: Scenario, scenario_name: str
) -> None:
    # Create a transform function when running in GitLab CI
    if 'CI_ARTIFACT_URL_PREFIX' in os.environ:
        artifact_url_prefix = os.environ['CI_ARTIFACT_URL_PREFIX']

        def transform_artifact_path(path: str) -> str:
            return '{}/{}'.format(artifact_url_prefix, os.path.relpath(path, start=os.curdir))

        run_scenario(citool, variables, scenario, scenario_name, 'pipeline', transform_artifact_path)

    else:
        run_scenario(citool, variables, scenario, scenario_name, 'pipeline')


@pytest.mark.parametrize(
    'scenario, scenario_name', load_scenarios('tests/pipeline', base_scenarios_dir_path='tests/base-scenarios')
)
def test_pipeline(
    citool: Tuple[CitoolRunnerType, str], variables: dict[str, Any], scenario: Scenario, scenario_name: str
) -> None:
    _run_scenario(citool, variables, scenario, scenario_name)


@pytest.mark.parametrize(
    'scenario, scenario_name', load_scenarios('tests/pipeline-invalid', base_scenarios_dir_path='tests/base-scenarios')
)
def test_pipeline_invalid(
    citool: Tuple[CitoolRunnerType, str], variables: dict[str, Any], scenario: Scenario, scenario_name: str
) -> None:
    exception, message = {
        'tests/pipeline-invalid/artifact.yaml': (
            AssertionError,
            "TEST: Artifact citool-config/variables.yaml exceeds max-size: \d+ byte > 64 byte",
        ),
        'tests/pipeline-invalid/glob-invalid.yaml': (
            AssertionError,
            "No file found required by check 'literal-strings\[citool-config/invalid-path\]'.",
        ),
    }[scenario_name]

    with pytest.raises(exception, match=message):
        _run_scenario(citool, variables, scenario, scenario_name)
