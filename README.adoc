:toc:

= Pytest Gluetool Plugin

Pytest plugin for creating integration tests for the https://gluetool.readthedocs.io/[Gluetool] framework.

Used for testing https://docs.testing-farm.io[Testing Farm] worker image.

== Development

=== Installation

If you want to develop this plugin, https://python-poetry.org/docs/#installation[Poetry] and https://pre-commit.com/#install[pre-commit] are required.

Then simply execute:

[source,shell]
....
make install
....

=== Tests

Testing of the project is currently done via:

* `pre-commit` - for static analysis, including mypy
* `pytest` - for functional tests

To run all functional tests:

[source,shell]
....
make test
....

To force running all pre-commit hooks:

[source,shell]
....
make pre-commit
....
