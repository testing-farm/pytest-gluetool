#!/bin/bash

if [ "$DEBUG" != "" ]; then
  set -x
fi

CITOOL_IMAGE="${CITOOL_IMAGE:-citool:deployed}"

echo "Known images:"
podman images

echo

image_id=$(podman inspect -f "{{.Id}}" $CITOOL_IMAGE)
ctime=$(podman inspect -f "{{.Created}}" $CITOOL_IMAGE)

echo "Image is $image_id, created on $ctime"

echo

export CITOOL_IMAGE
export CITOOL_IMAGE_ID="$image_id"
export CITOOL_IMAGE_CTIME="$ctime"

# * to avoid need of signal handling in this shell wrapper, run the podman command via `exec` so signals will target
#   directly podman, not this wrapper.
#
# * we'd like to have a proper init process in the container, to collect zombie processes. Without --init, zombies
#   would be stuck with pid 1 process - citool - until the container quits. For more details on zombies, see
#   https://blog.phusion.nl/2015/01/20/docker-and-the-pid-1-zombie-reaping-problem/
#
# * kerberos ccache dir is shared between multiple containers, thus *small* :z
exec podman run --init \
                --privileged \
                --rm \
                --network=host \
                -v $(readlink -f ${CITOOL_CONFIG}):/CONFIG:Z \
                -v ${WORKSPACE}:/var/ARTIFACTS:Z \
                ${CITOOL_EXTRA_PODMAN_ARGS} \
                "$CITOOL_IMAGE" "$@"
