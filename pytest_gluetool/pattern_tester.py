'''
Helper tool to run checks of pipeline output without the need to actually run
the pipelines. The only things needed are:
 * yaml file with integration test definition
 * directory with citool pipeline artifacts

USAGE: poetry run pattern-tester

TODO: add check for valid enclosure of jinja variables, i.e. {{<SPACE>VARIABLE<SPACE>}}
'''
import os
from typing import Optional, cast

import click
import gluetool

import pytest_gluetool.scenario


@click.command()
@click.option('--scenario', required=True, help='Path to yaml file with desired pipeline to test.')
@click.option('--artifacts-workdir', required=True, help='Path to directory with citool artifacts.')
@click.option(
    '--base-scenarios-dir-path',
    default=None,
    required=False,
    help='Path to directory with base scenarios the main scenario refers to.',
)
def main(scenario: str, artifacts_workdir: str, base_scenarios_dir_path: Optional[None]) -> None:
    stderr_filepath = os.path.join(artifacts_workdir, 'stderr.txt')
    scenarios = pytest_gluetool.scenario.load_scenarios(os.path.dirname(scenario), base_scenarios_dir_path)
    scenarios = [s for s in scenarios if s.id == scenario]

    if not scenarios:
        print('Scenario {} not found.'.format(scenario))
        return

    scenario_object = cast(pytest_gluetool.scenario.Scenario, scenarios[0].values[0])

    variables = {}

    # TODO: Variables are now implicitly read from artifacts directory, it would be nice to allow overwriting them via
    # an option to allow e.g. development of regexes in common.yaml file.
    if os.path.exists(os.path.join(artifacts_workdir, 'citool-config', 'variables.yaml')):
        variables.update(gluetool.utils.load_yaml(os.path.join(artifacts_workdir, 'citool-config', 'variables.yaml')))

    scenario_variables = gluetool.utils.dict_update({}, variables, scenario_object.content.get('variables', {}))

    checks: dict[str, list[str]] = scenario_object.content.get('checks', {})
    pytest_gluetool.scenario.perform_checks(
        checks, scenario_object, scenario_variables, artifacts_workdir, stderr_filepath
    )


if __name__ == '__main__':
    main()
