import configparser
import os
import shlex
import shutil
import stat
import subprocess
import tempfile
import threading
import time
from typing import Callable, Iterator, Optional, Union, cast

import _pytest
import _pytest.mark
import gluetool
import gluetool.utils
import py.path
import pytest

# Must be imported to get registered with pytest, otherwise we wouldn't get log capturing
from gluetool.tests.conftest import fixture_enable_logger  # noqa
from typing_extensions import Protocol

VariablesType = dict[str, Union[None, str, int, list[str], 'VariablesType']]

# Default container wrapper to use
CONTAINER_WRAPPER = os.path.join(os.path.dirname(__file__), 'citool-container-wrapper.sh')


class CitoolRunnerType(Protocol):
    def __call__(
        self,
        citool_options: Optional[list[str]] = None,
        pipeline: Union[None, str, list[str]] = None,
        env: Optional[dict[str, str]] = None,
        variables: Optional[VariablesType] = None,
    ) -> gluetool.utils.ProcessOutput:
        ...


def _patch_config(filepath: str, emitter: Callable[[], None]) -> None:
    """
    Replace a given file with another content. Backup file is made first, then given callback
    is called to modify the original file.
    """

    # Backup original file
    shutil.copy(filepath, '{}.backup-by-test'.format(filepath))

    emitter()


def _patch_artemis_user_data_vars(filepath: str, user_data_vars: list[str]) -> None:
    artemis_config = configparser.ConfigParser()
    artemis_config.read(filepath)

    _user_data_vars = user_data_vars.copy()

    # If the option exists, add our variables, otherwise initialize it.
    if artemis_config.has_option('default', 'user-data-vars'):
        _user_data_vars += [name.strip() for name in artemis_config.get('default', 'user-data-vars').split(',')]

    elif not artemis_config.has_section('default'):
        artemis_config.add_section('default')

    artemis_config.set('default', 'user-data-vars', ', '.join(_user_data_vars))

    with open(filepath, 'w') as f:
        artemis_config.write(f)


def _create_kerberos_ccache(keytab_filepath: str, keytab_principal: str) -> str:
    """
    Create directory with Kerberos ticket, and start a thread to refresh the ticket periodicaly.
    """

    # Create temporary directory under '/dev/shm' for storing ccache, as Ansible needs it to be tmpfs
    ccache_dirpath = tempfile.mkdtemp(dir='/dev/shm')

    # Since `kinit` would change permissions of ccache file, we need to create it under a different name,
    # fix permissions and replace the previous file with the new one.
    ccache_filepath = os.path.join(ccache_dirpath, 'ccache')
    ccache_new_filepath = '{}.new'.format(ccache_filepath)

    # Create local directory which will host ccache. This directory will be mounted to the citool containers.
    # Since the owner of the directory will not match the UID of processes running in the container, it must
    # be fully accessible even to Others. Otherwise we'd get complains by Ansible which also uses this directory.
    os.chmod(
        ccache_dirpath,
        stat.S_IRUSR
        | stat.S_IWUSR
        | stat.S_IXUSR
        | stat.S_IRGRP
        | stat.S_IWGRP
        | stat.S_IXGRP
        | stat.S_IROTH
        | stat.S_IWOTH
        | stat.S_IXOTH,
    )

    def _refresh() -> None:
        while True:
            subprocess.check_output(
                [
                    'kinit',
                    '-k',
                    '-t',
                    keytab_filepath,
                    '-c',
                    'FILE:{}'.format(ccache_new_filepath),
                    keytab_principal,
                ]
            )

            os.chmod(ccache_new_filepath, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)

            os.rename(ccache_new_filepath, ccache_filepath)

            time.sleep(3600)

    thread = threading.Thread(target=_refresh)
    thread.daemon = True
    thread.start()

    return ccache_dirpath


def pytest_configure(config: _pytest.config.Config) -> None:
    assert hasattr(config, '_metadata')

    config._metadata['variables'] = config.getoption('--variables')
    config._metadata['extra variables'] = config.getoption('--extra-var')
    config._metadata['artemis user data vars'] = config.getoption('--artemis-user-data-vars')
    config._metadata['citool image'] = config.getoption('--citool-image')
    config._metadata['citool config'] = config.getoption('--citool-config')
    config._metadata['citool container wrapper'] = config.getoption('--citool-container-wrapper')
    config._metadata['gluetool'] = config.getoption('--gluetool')
    config._metadata['gluetool modules'] = config.getoption('--gluetool-modules')
    config._metadata['gluetool modules internal'] = config.getoption('--gluetool-modules-internal')
    config._metadata['tests assets'] = config.getoption('--test-assets')
    config._metadata['citool extra podman args'] = config.getoption('--citool-extra-podman-args')

    print('Test run metadata:', gluetool.log.format_dict(config._metadata))

    # make sure citool does not run against production
    if config._metadata['citool config'] and os.path.exists(
        os.path.join(config._metadata['citool config'], '.production')
    ):
        assert False, 'It is forbidden to run integration tests against production!'


def pytest_addoption(parser: _pytest.config.argparsing.Parser) -> None:
    parser.addoption(
        '--variables',
        action='append',
        type=str,
        default=[],
        help='YAML file with additional variables, e.g. image names, build targets and so on.',
    )

    parser.addoption(
        '--extra-var',
        action='append',
        type=str,
        default=[],
        help='Change existing variable, e.g. one from variables.yaml. NAME=VALUE, NAME1.NAME2=VALUE',
    )

    parser.addoption(
        '--artemis-user-data-vars',
        action='append',
        type=str,
        default=[],
        help='Specify list of comma-delimited variable names to be passed as Artemis user data vars',
    )

    parser.addoption(
        '--citool-runner',
        action='store',
        type=str,
        default='container',
        choices=('container', 'virtualenv'),
        help='Which citool runner to use - `container` (default) or `virtualenv`',
    )

    parser.addoption(
        '--citool-virtualenv',
        action='store',
        type=str,
        default=None,
        help='citool virtualenv to test (needs gluetool & citool installed)',
    )

    parser.addoption(
        '--gluetool-modules',
        action='store',
        type=str,
        default=None,
        help='gluetool modules (upstream) to load instead of default ones',
    )

    parser.addoption(
        '--gluetool-modules-internal',
        action='store',
        type=str,
        default=None,
        help='gluetool modules (internal) to load instead of default ones',
    )

    parser.addoption('--gluetool', action='store', type=str, default=None, help='Replacement of gluetool package')

    parser.addoption('--citool-image', action='store', type=str, default=None, help='citool Podman image to test')

    parser.addoption(
        '--citool-config', action='store', type=str, default=None, help='citool configuration to use for testing'
    )

    parser.addoption(
        '--citool-container-wrapper',
        action='store',
        type=str,
        default=CONTAINER_WRAPPER,
        help='path to shell script which runs the citool container, by default use {}'.format(CONTAINER_WRAPPER),
    )

    parser.addoption(
        '--kerberos-keytab', action='store', type=str, help='Initialize kerberos ticket from the given kerberos keytab'
    )

    parser.addoption('--kerberos-principal', action='store', type=str, help='Kerberos principal of the keytab')

    parser.addoption(
        '--test-assets',
        action='store',
        type=str,
        help='path to test assets',
    )

    parser.addoption(
        '--citool-extra-podman-args',
        action='append',
        type=str,
        help='extra command line arguments to be passed to podman in citool-container-wrapper.sh script',
    )


@pytest.fixture(name='variables')
def fixture_variables(request: _pytest.fixtures.FixtureRequest) -> VariablesType:
    variables: VariablesType = {}

    # First, load all files specified by the user, and merge them - in the order - into a single tree of variables.
    for variables_filepath in request.config.getoption('--variables'):
        variables.update(gluetool.utils.load_yaml(variables_filepath))

    # Then take extra variables, and update the stash by changing respective leaves.
    for var_spec in cast(list[str], request.config.getoption('--extra-var')):
        try:
            path, value = var_spec.split('=', 1)

        except ValueError:
            assert False, 'Extra variable "{}" does not fit NAME=VALUE format'.format(var_spec)

        # Walk the path down to the last item, and pick each container with variables. Quit before
        # the last one because we don't want to "dereference" the last step, it is a name in the
        # container we want to override.
        steps = path.split('.')
        container: VariablesType = variables

        while len(steps) > 1:
            step = steps.pop(0)

            if step not in container:
                # We *may* chose to let this slip and *set* the variable instead, but at this moment we don't
                # have use case for that.
                assert False, 'Step "{}" of extra variable "{}" does not exist'.format(step, var_spec)

            container_step = container[step]

            if not isinstance(container_step, dict):
                assert False, 'Step "{}" of extra variable "{}" is a leaf key'.format(step, var_spec)

            container = container_step

        container[steps.pop()] = value.strip()

    return variables


@pytest.fixture(name='artemis_user_data_vars')
def fixture_artemis_user_data_vars(request: _pytest.fixtures.FixtureRequest) -> list[str]:
    return gluetool.utils.normalize_multistring_option(request.config.getoption('--artemis-user-data-vars'))


@pytest.fixture(name='citool_virtualenv')
def fixture_citool_virtualenv(request: _pytest.fixtures.FixtureRequest) -> str:
    return cast(str, request.config.getoption('--citool-virtualenv'))


@pytest.fixture(name='citool_image')
def fixture_citool_image(request: _pytest.fixtures.FixtureRequest) -> str:
    return cast(str, request.config.getoption('--citool-image'))


@pytest.fixture(name='citool_config')
def fixture_citool_config(request: _pytest.fixtures.FixtureRequest) -> str:
    return cast(str, os.path.abspath(request.config.getoption('--citool-config')))


@pytest.fixture(name='kerberos_keytab')
def fixture_kerberos_keytab(request: _pytest.fixtures.FixtureRequest) -> Optional[str]:
    if not request.config.getoption('--kerberos-keytab'):
        return None

    return cast(str, os.path.abspath(request.config.getoption('--kerberos-keytab')))


@pytest.fixture(name='kerberos_principal')
def fixture_kerberos_principal(request: _pytest.fixtures.FixtureRequest) -> Optional[str]:
    if not request.config.getoption('--kerberos-principal'):
        return None

    return cast(str, os.path.abspath(request.config.getoption('--kerberos-principal')))


@pytest.fixture(name='citool_container_wrapper')
def fixture_citool_container_wrapper(request: _pytest.fixtures.FixtureRequest) -> Optional[str]:
    if not request.config.getoption('--citool-container-wrapper'):
        return None

    return cast(str, os.path.abspath(request.config.getoption('--citool-container-wrapper')))


@pytest.fixture(name='test_assets')
def fixture_test_assets(request: _pytest.fixtures.FixtureRequest) -> Optional[str]:
    if not request.config.getoption('--test-assets'):
        return None

    return cast(str, os.path.abspath(request.config.getoption('--test-assets')))


@pytest.fixture(name='citool_extra_podman_args')
def fixture_citool_extra_podman_args(request: _pytest.fixtures.FixtureRequest) -> list[str]:
    return gluetool.utils.normalize_multistring_option(request.config.getoption('--citool-extra-podman-args'))


@pytest.fixture(name='smtp')
def fixture_smtp(citool_config: str, smtpserver: str, tmpdir: py.path.local) -> Iterator[None]:
    # Disable local SMTP server temporarily - we're using "check the logs" output instead of capturing
    # the actual SMTP traffic.
    yield None

    return

    config_root = py.path.local(citool_config)

    notify_email_config = config_root.join('config', 'smtp')
    saved_notify_email_config = None

    _, smtp_port = smtpserver.addr

    if notify_email_config.check():
        saved_notify_email_config = tmpdir.join('smtp.bak')
        notify_email_config.copy(saved_notify_email_config)

        with notify_email_config.open('a') as f:
            print('smtp-port = {}'.format(smtp_port), file=f)

    else:
        with notify_email_config.open('w') as f:
            print('[default]\nsmtp-port={}'.format(smtp_port), file=f)

    yield smtpserver

    if saved_notify_email_config:
        saved_notify_email_config.copy(notify_email_config)

    else:
        notify_email_config.remove(rec=False)


def _read_jenkins_config(citool_config: str) -> Optional[configparser.ConfigParser]:
    # Simulates environment set by Jenkins for the build
    jenkins_config = configparser.ConfigParser()
    jenkins_config.read(os.path.join(citool_config, 'config', 'jenkins'))

    if not jenkins_config.sections():
        return None
    return jenkins_config


def _create_citool_runner_virtualenv(
    request: _pytest.fixtures.FixtureRequest,
    smtp: None,
    citool_virtualenv: str,
    citool_config: str,
    artemis_user_data_vars: list[str],
    tmpdir: py.path.local,
) -> tuple[CitoolRunnerType, str]:
    # Save a copy of given configuration in the pytest working directory
    local_citool_config = os.path.join(str(tmpdir), 'citool-config')
    shutil.copytree(citool_config, local_citool_config, ignore=shutil.ignore_patterns('.git'))
    os.symlink('citool-config', os.path.join(str(tmpdir), '.gluetool.d'))

    base_env = dict(os.environ)

    base_env.update(
        {
            'CITOOL_CONFIG': local_citool_config,
            'DEBUG': '0',
            'WORKSPACE': str(tmpdir),
            # TODO: gluetool probably does not handle `SENTRY_TAG_MAP=` - needs more experiments
            # but without this line citool fails to init its Sentry integration
            'SENTRY_TAG_MAP': 'foo=bar',
        }
    )

    trampoline = tmpdir.join('trampoline.sh')

    def run_citool(
        citool_options: Optional[list[str]] = None,
        pipeline: Union[None, str, list[str]] = None,
        env: Optional[dict[str, str]] = None,
        variables: Optional[VariablesType] = None,
    ) -> gluetool.utils.ProcessOutput:
        citool_options = citool_options or []
        pipeline = pipeline or []
        env = env or {}

        base_env.update(env)

        # Make sure Artemis module propagates the interesting variables down to its requests
        artemis_config_filepath = os.path.join(local_citool_config, 'config', 'artemis')

        _patch_artemis_user_data_vars(artemis_config_filepath, artemis_user_data_vars)

        if isinstance(citool_options, str):
            citool_options = shlex.split(citool_options)

        if isinstance(pipeline, str):
            pipeline = shlex.split(pipeline.replace('\n', ' '))

        # Update variables.yaml with given variables
        variables_filepath = os.path.join(local_citool_config, 'variables.yaml')

        if os.path.exists(variables_filepath):
            _patch_config(variables_filepath, lambda: gluetool.utils.dump_yaml(variables, variables_filepath))

        trampoline.write(
            """#!/bin/bash
. {VIRTUALENV}/bin/activate

{VIRTUALENV}/bin/citool "$@"
""".format(
                VIRTUALENV=citool_virtualenv
            )
        )

        trampoline.chmod(0o544)

        cmd = gluetool.utils.Command([str(trampoline)], options=citool_options + pipeline)

        return cmd.run(cwd=str(tmpdir), env=base_env)

    return run_citool, str(tmpdir)


def _create_citool_runner_container(
    request: _pytest.fixtures.FixtureRequest,
    smtp: None,
    citool_image: str,
    citool_config: str,
    citool_container_wrapper: str,
    artemis_user_data_vars: list[str],
    test_assets: str,
    kerberos_keytab: Optional[str],
    kerberos_principal: Optional[str],
    tmpdir: py.path.local,
    citool_extra_podman_args: list[str],
) -> tuple[CitoolRunnerType, str]:
    # Save a copy of given configuration - we may need to modify it, and it's always better to have our own
    # copy anyway, for debugging purposes.
    local_citool_config = os.path.join(str(tmpdir), 'citool-config')
    shutil.copytree(citool_config, local_citool_config, ignore=shutil.ignore_patterns('.git'))

    base_env = dict(os.environ)

    if kerberos_keytab and kerberos_principal:
        ccache_dirpath = _create_kerberos_ccache(kerberos_keytab, kerberos_principal)
        base_env.update(
            {
                'KERBEROS_CCACHE_DIR': ccache_dirpath,
            }
        )

    citool_extra_podman_args += [
        # Without this, we cannot mount ~/.citool.d/ to multiple Podman containers at once,
        # which ruins our parallelization experience.
        '--security-opt label=disable',
        # add assets directory for additional data which can be used by the modules
        '-v {}:/ASSETS:Z'.format(test_assets),
    ]

    def absolute_path(path: str) -> str:
        return os.path.abspath(os.path.expanduser(path))

    if request.config.getoption('--gluetool'):
        citool_extra_podman_args += [
            '-v {}:/opt/gluetool/lib/python3.9/site-packages/gluetool/:Z'.format(
                absolute_path(request.config.getoption('--gluetool'))
            )
        ]

    if request.config.getoption('--gluetool-modules'):
        citool_extra_podman_args += [
            '-v {}:/opt/gluetool/lib/python3.9/site-packages/gluetool_modules_framework:Z'.format(
                absolute_path(request.config.getoption('--gluetool-modules'))
            )
        ]

    if request.config.getoption('--gluetool-modules-internal'):
        citool_extra_podman_args += [
            '-v {}:/gluetool-modules/gluetool_modules_internal:Z'.format(
                absolute_path(request.config.getoption('--gluetool-modules-internal'))
            )
        ]

    base_env.update(
        {
            'ANSIBLE_HOST_KEY_CHECKING': 'False',
            'ANSIBLE_PIPELINING': 'True',
            'CITOOL_IMAGE': citool_image,
            'CITOOL_CONFIG': local_citool_config,
            'DEBUG': 'yes',
            'WORKSPACE': str(tmpdir),
            'CITOOL_EXTRA_PODMAN_ARGS': ' '.join(citool_extra_podman_args),
            # TODO: gluetool probably does not handle `SENTRY_TAG_MAP=` - needs more experiments
            # but without this line citool fails to init its Sentry integration
            'SENTRY_TAG_MAP': 'foo=bar',
        }
    )

    # make tmp dir accessible to the citool inside the image - :Z in the volume mount is not enough,
    # that's just to fool SELinux
    tmpdir.chmod(0o777)

    def run_citool(
        citool_options: Optional[list[str]] = None,
        pipeline: Union[None, str, list[str]] = None,
        env: Optional[dict[str, str]] = None,
        variables: Optional[VariablesType] = None,
    ) -> gluetool.utils.ProcessOutput:
        citool_options = citool_options or []
        pipeline = pipeline or []
        env = env or {}
        variables = variables or {}

        base_env.update(env)

        # Update variables.yaml with given variables
        variables_filepath = os.path.join(local_citool_config, 'variables.yaml')

        if os.path.exists(variables_filepath):
            _patch_config(variables_filepath, lambda: gluetool.utils.dump_yaml(variables, variables_filepath))

        # Make sure Artemis module propagates the interesting variables down to its requests
        artemis_config_filepath = os.path.join(local_citool_config, 'config', 'artemis')

        _patch_artemis_user_data_vars(artemis_config_filepath, artemis_user_data_vars)

        if isinstance(citool_options, str):
            citool_options = shlex.split(citool_options)

        if isinstance(pipeline, str):
            pipeline = shlex.split(pipeline.replace('\n', ' '))

        cmd = gluetool.utils.Command([citool_container_wrapper], options=citool_options + pipeline)

        return cmd.run(env=base_env)

    return run_citool, str(tmpdir)


@pytest.fixture(name='citool')
def fixture_citool(
    request: _pytest.fixtures.FixtureRequest,
    smtp: None,
    citool_virtualenv: str,
    citool_image: str,
    citool_config: str,
    citool_container_wrapper: str,
    artemis_user_data_vars: list[str],
    test_assets: Optional[str],
    kerberos_keytab: Optional[str],
    kerberos_principal: Optional[str],
    tmpdir: py.path.local,
    citool_extra_podman_args: list[str],
) -> tuple[CitoolRunnerType, str]:
    runner = request.config.getoption('--citool-runner')

    if not test_assets:
        assert False, 'No test assets specified, set them via --test-assets'

    if runner == 'container':
        if not citool_image:
            assert False, 'This test requires an image to test, set via --citool-image'

        return _create_citool_runner_container(
            request,
            smtp,
            citool_image,
            citool_config,
            citool_container_wrapper,
            artemis_user_data_vars,
            test_assets,
            kerberos_keytab,
            kerberos_principal,
            tmpdir,
            citool_extra_podman_args,
        )

    if runner == 'virtualenv':
        if not citool_virtualenv:
            assert False, 'This test requires an virtualenv to test, set via --citool-virtualenv'

        return _create_citool_runner_virtualenv(
            request, smtp, citool_virtualenv, citool_config, artemis_user_data_vars, tmpdir
        )

    # Shouldn't be reachable, given the possible choices for runner.
    assert False, "Unhandled runner choice, '{}'".format(runner)
