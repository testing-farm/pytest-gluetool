import glob
import inspect
import os
import re
import subprocess
from dataclasses import dataclass
from typing import Any, Callable, Dict, Iterator, List, Optional, Pattern, Union

import _pytest
import gluetool
import jinja2.defaults
import pint
import pytest
from _pytest.mark.structures import ParameterSet
from gluetool.color import Colors
from gluetool.glue import GlueError
from gluetool.log import format_table
from gluetool.utils import render_template

from pytest_gluetool import CitoolRunnerType, VariablesType

UNITS = pint.UnitRegistry()


@dataclass
class Scenario:
    content: Dict[str, Any]
    base_scenarios_dir_path: Optional[str] = None


def regex_escape_filter(s: str) -> str:
    return re.escape(s)


jinja2.defaults.DEFAULT_FILTERS['regex_escape'] = regex_escape_filter


def sub_patterns(pattern: str) -> Iterator[tuple[int, Pattern[str]]]:
    """
    Yield all sub-patterns, generated from a given pattern by removing last characters.
    """

    for offset in range(len(pattern) + 1, 0, -1):
        try:
            yield offset, re.compile(pattern[:offset])

        except re.error:
            continue


def partial_pattern_match(pattern: str, text: str) -> tuple[Optional[int], Optional[int]]:
    """
    Try to find out how far can a given pattern match a text. Pattern is continuously being shortened by
    one character until we get to the first pattern that matches.
    """

    for subpattern_offset, subpattern in sub_patterns(pattern):
        match = subpattern.search(text)

        if not match:
            continue

        return subpattern_offset, match.end()

    return None, None


def get_scenario_marks(scenario: Scenario) -> list[_pytest.mark.structures.Mark]:
    """
    From scenario description - loaded from a YAML file - extract pytest marks, and convert them
    to their standard pytest object representation using ``pytest.mark``.
    """

    marks = []

    for mark in scenario.content.get('marks', []):
        # Simple string represents a name, no additional parameters => return `pytest.mark.$name` mark.
        if isinstance(mark, str):
            marks.append(getattr(pytest.mark, mark))

        # Dictionary provides a name and remaining fields are to be used as keyword parameters
        # of `pytest.mark.$name`, creating new mark object.
        elif isinstance(mark, dict):
            mark_name = mark.pop('name')

            marks.append(getattr(pytest.mark, mark_name)(**mark))

    # If there's a checks.messages key, it means the scenario was described using the deprecated
    # format of checks - SKIP it, and warn user to apply update.
    if scenario.content.get('checks', {}).get('messages', []):
        marks.append(pytest.mark.skip(reason='Old-style scenario format, please update!'))

    return marks


def _merge_entity(entity: Any, base_entity: Any) -> None:
    """
    Recursively merge dicts and lists.
    Matched keys in dicts and str will be overwritten by value in entity.
    """
    if isinstance(base_entity, dict):
        for k, v in base_entity.items():
            if k in entity:
                _merge_entity(entity.get(k), v)
            else:
                entity[k] = v
    elif isinstance(base_entity, list):
        entity += base_entity
    else:
        return


def compose_scenario(scenario_filepath: str, scenario: Scenario, base_scenarios: List[Dict[str, Any]]) -> Scenario:
    for base_scenario_name in scenario.content['extends']:
        matched_base_scenarios = [
            base_scenario for base_scenario in base_scenarios if base_scenario['name'] == base_scenario_name
        ]

        if len(matched_base_scenarios) > 1:
            raise GlueError(
                'A base scenario with the "{}" name referred by "{}" was not found'.format(
                    base_scenario_name,
                    scenario_filepath,
                )
            )

        elif len(matched_base_scenarios) == 0:
            raise GlueError('A base scenario with the "{}" name is not found'.format(base_scenario_name))

        else:
            _merge_entity(scenario.content, matched_base_scenarios[0])

    return scenario


def load_scenarios(source_dir_path: str, base_scenarios_dir_path: Optional[str] = None) -> list[ParameterSet]:
    """
    Given the directory path, load all scenarios from the directory, and convert them to
    structure pytest understands, used to parametrize a test function.
    Any file with '.yml' or '.yaml' suffix is consider a scenario.

    :returns: List of ``pytest.param`` instances, each is given two parameters: scenario description
        as a Scenario object, and scenario name (which is the name of the YAML file).
    """

    scenarios = []
    base_scenarios = []

    if base_scenarios_dir_path:
        for base_scenario_name in sorted(os.listdir(base_scenarios_dir_path)):
            base_scenario = gluetool.utils.load_yaml(os.path.join(base_scenarios_dir_path, base_scenario_name))

            if not base_scenario.get('name'):
                raise GlueError(
                    'The "{}" base scenario has no defined name'.format(
                        os.path.join(base_scenarios_dir_path, base_scenario_name)
                    )
                )

            base_scenarios.append(base_scenario)

    for scenario_name in sorted(os.listdir(source_dir_path)):
        # Skip:
        #
        # * everything that's not YAML
        # * everything hidden
        # * everything with ".spec." substring - these are often used as source templates for actual scenarios.
        if (
            scenario_name[0] == '.'
            or (scenario_name[-4:] != '.yml' and scenario_name[-5:] != '.yaml')
            or '.spec.' in scenario_name
            or '.template' in scenario_name
        ):
            continue

        scenario_filepath = os.path.join(source_dir_path, scenario_name)
        scenario = Scenario(
            content=gluetool.utils.load_yaml(scenario_filepath), base_scenarios_dir_path=base_scenarios_dir_path
        )

        if base_scenarios and scenario.content.get('extends'):
            scenario = compose_scenario(scenario_filepath, scenario, base_scenarios)

        scenarios.append(
            pytest.param(
                scenario,
                str(os.path.join(source_dir_path, scenario_name)),
                id=str(os.path.join(source_dir_path, scenario_name)),
                marks=get_scenario_marks(scenario),
            )
        )

    return scenarios


def ASSET(*pieces: str) -> str:
    return os.path.join(os.getcwd(), 'tests', 'assets', *pieces)


def JSON(*pieces: str) -> Callable[[], Any]:
    asset_path = ASSET(*pieces)

    def getter() -> Any:
        return gluetool.utils.load_json(asset_path)

    getter.__name__ = os.path.join(*pieces)
    getter.filename = pieces[-1]  # type: ignore
    getter.filepath = os.path.abspath(asset_path)  # type: ignore

    return getter


def YAML(*pieces: str) -> Callable[[], Any]:
    asset_path = ASSET(*pieces)

    def getter() -> Any:
        return gluetool.utils.load_yaml(asset_path)

    getter.__name__ = os.path.join(*pieces)
    getter.filename = pieces[-1]  # type: ignore
    getter.filepath = os.path.abspath(asset_path)  # type: ignore

    return getter


def assert_citool_messages(
    output_name: str,
    output: str,
    expected_messages: list[str],
    variables: VariablesType,
    failures: list[str],
    regex: bool = False,
    literal: bool = False,
) -> None:
    # Render a string given a template. Apply all escaping and variable replacements if necessary.
    # Returns the rendered template and the original string.
    def _render_message_test_template(template: str) -> tuple[str, str]:
        template = template.strip()
        original_template = template

        if literal:
            # We were asked to escape all significant characters. So, let's do it.
            template = re.escape(template)

            # But because of that, we also broken possible templates: `{{ foo }}` have been escaped to `\{\{\ foo\ \}\}`
            # which won't get replaced by the actual value.
            template = template.replace(r'\{', r'{').replace(r'\}', r'}')

            # Ain't over yet, now we're dealing with this: `{{\\ foo\\_bar\\ }}` or even `{{\\ \\'\\\\d\\+\\'\\ }}`.
            for var in re.findall(r"\{\{\\ .*?\\ \}\}", template):
                var_fixed = var
                for k, v in {
                    '\\ ': ' ',
                    '\\_': '_',
                    '\\.': '.',
                    "\\'": "'",
                    '\\|': '|',
                    '\\+': '+',
                    '\\?': '?',
                    '\\*': '*',
                    '\\[': '[',
                    '\\]': ']',
                    '\\(': '(',
                    '\\)': ')',
                    '\\-': '-',
                    '\\:': ':',
                    '\\#': '#',
                }.items():
                    var_fixed = var_fixed.replace(k, v)

                template = template.replace(var, var_fixed)

        if variables:
            return gluetool.utils.render_template(template, logger=None, **variables), original_template

        return template, original_template

    # Apply a simple "find the text" test on the output. Obeys regex and literal toggles,
    # any failure is added to the list.
    def _run_straight_test(expected_message: str, expected_message_template: str) -> None:
        if regex:
            # In case the pattern cannot be even compiled, print a reasonable error
            try:
                expected_message_regex = re.compile(expected_message, flags=re.M)

            except re.error:
                failures.append('TEST: Pattern {} is an invalid regular expression'.format(expected_message))
                return

            match = expected_message_regex.search(output)

            if match is None:
                subpattern_offset, text_offset = partial_pattern_match(expected_message, output)

                if literal:
                    failure = inspect.cleandoc(
                        """
                        TEST: Literal pattern not found in: {}

                        {}

                            Rendered pattern:

                        {}

                            Pattern with the longest possible match was:

                        {}
                        """
                    ).format(
                        output_name,
                        expected_message_template,
                        expected_message,
                        expected_message[:subpattern_offset] if subpattern_offset else 'no match at all...',
                    )

                else:
                    failure = inspect.cleandoc(
                        """
                        TEST: Pattern not found in {}: {}

                            Pattern with the longest possible match was: {}
                        """
                    ).format(
                        output_name,
                        expected_message,
                        expected_message[:subpattern_offset] if subpattern_offset else 'no match at all...',
                    )

                failures.append(failure)

            else:
                if literal:
                    print('TEST: Literal pattern found in {}:\n{}'.format(output_name, expected_message_template))
                    print()

                else:
                    print("TEST: Pattern found in {}: {}".format(output_name, expected_message))
                    print()

        elif expected_message not in output:
            failures.append("TEST: String not found in {}: {}".format(output_name, expected_message))

        else:
            print("TEST: String found in {}: {}".format(output_name, expected_message))
            print()

    # Apply a complex, "find table in text" test. Each row and each cell is treated as a simple test from above,
    # a complex pattern is constructed from these cells.
    def _run_table_test(table_test: list[str]) -> None:
        rows: list[tuple[str, str]] = []

        for cell_templates in table_test:
            cells = [r'']
            original_cell_templates = ['']

            for cell_template in cell_templates:
                cell, cell_template = _render_message_test_template(cell_template)

                cells += [r'\s{}\s+'.format(cell)]

                original_cell_templates += [' {} '.format(cell_template)]

            cells += [r'']
            original_cell_templates += ['']

            rows += [(r'\|'.join(cells), '|'.join(original_cell_templates))]

        rows_pattern = '\n'.join([row for row, _ in rows])
        rows_template = '\n'.join([row_template for _, row_template in rows])

        _run_straight_test(rows_pattern, rows_template)

    for test_template in expected_messages:
        if isinstance(test_template, str):
            expected_message, original_expected_message_template = _render_message_test_template(test_template)

            _run_straight_test(expected_message, original_expected_message_template)

        elif isinstance(test_template, dict):
            if 'table' in test_template:
                _run_table_test(test_template['table'])

            else:
                assert 'unhandled test template: {}'.format(test_template)


def assert_citool_artifacts(
    workdir: str, expected_artifacts: list[Union[str, dict[str, str]]], variables: VariablesType, failures: list[str]
) -> None:
    # Remove workdir from the filepath and use `lstrip('/') because it's possible that there are multiple `/`s
    # separating the workdir from the files inside.
    # TODO: This double `/` might be caused by an implementation of `transform_artifact_path` function,
    # so investigate there.
    directory_content = [
        os.path.join(dirpath[len(workdir) :].lstrip('/'), filename)
        for dirpath, _, filenames in os.walk(workdir)
        for filename in filenames
    ]

    for expected_artifact in expected_artifacts:
        expected_artifact_path_template = (
            expected_artifact['path'] if isinstance(expected_artifact, dict) else expected_artifact
        )
        expected_artifact_path = render_template(expected_artifact_path_template, logger=None, **variables)

        matches_list = list(filter(re.compile(expected_artifact_path).match, directory_content))

        if len(matches_list) <= 0:
            failures.append("TEST: Artifact is not in work directory: {}".format(expected_artifact_path))

        else:
            print("TEST: Artifact is in work directory: {}".format(expected_artifact_path))
            print()

        if isinstance(expected_artifact, dict) and 'max-size' in expected_artifact:
            max_size = UNITS(expected_artifact['max-size'])

            for file_name in matches_list:
                file_size = UNITS.Quantity(os.path.getsize(os.path.join(workdir, file_name)), 'byte')

                if file_size > max_size:
                    failures.append(
                        'TEST: Artifact {} exceeds max-size: {} > {}'.format(file_name, file_size, max_size)
                    )
                else:
                    print('TEST: Artifact {} does not exceed max-size: {} < {}'.format(file_name, file_size, max_size))
                    print()


def _do_assert(failures: list[str]) -> None:
    messages: list[str] = []

    for failure in failures:
        messages += ['', gluetool.log.BLOB_HEADER, '', '    {}'.format(failure), '', gluetool.log.BLOB_FOOTER, '']

    assert False, '\n\n'.join(messages)


def run_scenario(
    citool: tuple[CitoolRunnerType, str],
    variables: VariablesType,
    scenario: Scenario,
    scenario_filepath: str,
    scenario_type: str,
    transform_artifact_path: Callable[[str], str] = lambda path: path,
) -> None:
    """
    Run a pipeline test scenario. Given the description of the scenario, run a pipeline and check
    its output and results.
    """

    citool_runner, workdir = citool

    # Variables relevant for the scenario - mixture of global variables (via --variables) and those
    # set by the scenario (`variables` key)
    scenario_variables: VariablesType = {
        '__TEST_SCENARIO__': scenario_filepath,
        '__TEST_SCENARIO_TYPE__': scenario_type,
    }

    scenario_variables.update(variables)
    scenario_variables.update(scenario.content.get('variables', {}))

    # Put checks aside - we will use them for printing and for checking later, good to have them
    # as a helper variable.
    checks = scenario.content.get('checks', {})

    stdout_filepath = os.path.join(workdir, 'stdout.txt')
    stderr_filepath = os.path.join(workdir, 'stderr.txt')
    stderr_html_filepath = os.path.join(workdir, 'stderr.html')

    # Print some interesting information about the scenario, to make debugging easier
    scenario_info_table = [
        ['Scenario', scenario_filepath],
        ['Workdir', transform_artifact_path(workdir)],
        ['Output', transform_artifact_path(stderr_filepath)],
        ['Output as HTML', transform_artifact_path(stderr_html_filepath)],
        ['Debug log', transform_artifact_path(os.path.join(workdir, 'citool-debug.txt'))],
        ['Configuration', transform_artifact_path(os.path.join(workdir, 'citool-config'))],
        ['Variables', transform_artifact_path(os.path.join(workdir, 'citool-config', 'variables.yaml'))],
        [
            'Reproducer',
            'pattern-tester --scenario {} --artifacts-workdir {} {}'.format(
                scenario_filepath,
                transform_artifact_path(workdir),
                '--base-scenarios-dir-path {}'.format(scenario.base_scenarios_dir_path)
                if scenario.base_scenarios_dir_path
                else '',
            ),
        ],
    ]

    gluetool.color.switch(True)

    def _colorize(s: str) -> str:
        assert Colors.style
        return Colors.style(s, fg='green')

    print('~~~~~ ~~~~~ ~~~~~ SCENARIO ~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~')
    print(format_table(scenario_info_table, tablefmt='psql', headers='firstrow'))
    # print
    # print _colorize('Pipeline to execute:')
    # print _colorize(scenario['pipeline'])
    # print
    # print _colorize('Checks:')
    # print _colorize(format_dict(checks))
    # print
    # print _colorize('Scenario variables:'), format_dict(scenario_variables)
    print('~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~')

    # The actual pipeline to run - render it, to fill in blanks with variables
    # (mypy reports some weird type mismatch, assigning `scenario_variables` to `logger` which seems to work elsewhere)
    pipeline = render_template(scenario.content['pipeline'], logger=None, **scenario_variables)

    # Run the pipeline
    try:
        output = citool_runner(
            citool_options=scenario.content.get('citool-options', ''),
            pipeline=pipeline,
            env=scenario.content.get('env', None),
            variables=scenario_variables,
        )

    except gluetool.glue.GlueCommandError as exc:
        output = exc.output

    if output.stdout:
        with open(stdout_filepath, 'w') as f:
            f.write(output.stdout)
            f.flush()

    if output.stderr:
        with open(stderr_filepath, 'w') as f:
            f.write(output.stderr)
            f.flush()

    # TODO: some commands might not be available, running it in a try block
    try:
        subprocess.check_call(['bash', '-c', 'cat {} | ansi2html > {}'.format(stderr_filepath, stderr_html_filepath)])
    except subprocess.CalledProcessError:
        pass

    print('~~~~~ ~~~~~ ~~~~~ OUTPUT ~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~')
    print(output.stderr or '')
    print('~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~')

    # And now the actual work, making sure things turned out as expected
    perform_checks(checks, scenario, scenario_variables, workdir, stderr_filepath, output)


def perform_checks(
    checks: dict[str, Any],
    scenario: Scenario,
    scenario_variables: VariablesType,
    workdir: str,
    stderr_filepath: str,
    output: Any = None,
) -> None:
    # Keep put all failed asserts into a single huge pile, and show them all at once
    # instead of stopping at first error. Since we're working with matching lines in
    # the output, it makes more sense to display all failed patterns that just the first
    # one, forcing user to restart test and wait for another bad line.
    failures = []

    def _get_target_filepaths(s: str) -> list[tuple[str, str]]:
        match = re.match(r'.*?\[(.*?)\]', s)

        if not match:
            return [('stderr.txt', stderr_filepath)]

        paths = glob.glob(os.path.join(workdir, match.group(1).strip()))

        return [(os.path.relpath(path, workdir), path) for path in paths]

    def _get_target_content(s: str) -> list[tuple[str, str, str]]:
        names_filepaths = _get_target_filepaths(s)

        if not names_filepaths:
            failures.append("No file found required by check '{}'.".format(s))

        output = []
        for name, filepath in names_filepaths:
            with open(filepath, 'r') as f:
                output.append((name, filepath, f.read()))
        return output

    for check_target, check_list in checks.items():
        if check_target.startswith('patterns'):
            for name, filepath, content in _get_target_content(check_target):
                assert_citool_messages(name, content, check_list, scenario_variables, failures, regex=True)

        elif check_target.startswith('literal-patterns'):
            for name, filepath, content in _get_target_content(check_target):
                assert_citool_messages(
                    name, content, check_list, scenario_variables, failures, regex=True, literal=True
                )

        elif check_target.startswith('literal-strings'):
            for name, filepath, content in _get_target_content(check_target):
                assert_citool_messages(name, content, check_list, scenario_variables, failures)

        elif check_target == 'artifacts':
            assert_citool_artifacts(workdir, check_list, scenario_variables, failures)

    if output and output.exit_code != 0 and not scenario.content.get('expected_failure', False):
        failures.insert(0, 'Pipeline did not finish successfully!')

    if output and output.exit_code == 0 and scenario.content.get('expected_failure', False):
        failures.insert(0, 'Pipeline did finish successfully, but is expected to fail!')

    if failures:
        _do_assert(failures)
