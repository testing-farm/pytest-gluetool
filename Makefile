.DEFAULT_GOAL := help

.PHONY: clean help install test pre-commit

VENV := $(shell poetry run true &>/dev/null && poetry env info -p)
ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

# container image used for testing
# TODO: use a stable worker image here later
TESTING_FARM_IMAGE := quay.io/testing-farm/worker:latest

# autodetect python version rather than hardcoding it
PYTHON_MAJOR_MINOR := $(shell poetry run python -c "import sys;print(f'{sys.version_info.major}.{sys.version_info.minor}')")

# we use the module directory to detect if pytest-gluetool is installed
INSTALLED := $(shell find $(VENV)/lib/python$(PYTHON_MAJOR_MINOR)/site-packages/pytest_gluetool* -type d 2>/dev/null || echo __NOT_INSTALLED__)

##@ Setup

install:  ## Install pre-commit and prepare development environment
	pre-commit install
	poetry install -E development

##@ Tests

$(INSTALLED):
	poetry install -E development

test: $(INSTALLED)  ## Run available tests
	poetry run pytest --basetemp .pytest -vv -ra \
	--citool-config $(ROOT_DIR)/tests/.gluetool.d --citool-virtualenv $(VENV) --citool-runner virtualenv \
	--artemis-user-data-vars __TEST_VAR__ \
	--test-assets $(ROOT_DIR)/tests \
	--variables $(ROOT_DIR)/tests/.gluetool.d/variables.yaml --variables tests/common.yaml \
	--html report.html

pre-commit: $(INSTALLED)  ## Run pre-commit on all files
	pre-commit run --all-files

test-container: $(INSTALLED)  ## Run available tests against a container
	poetry run pytest --basetemp .pytest -vv -ra \
	--citool-config $(ROOT_DIR)/tests/.gluetool.d --citool-virtualenv $(VENV) --citool-runner container --citool-image $(TESTING_FARM_IMAGE) \
	--artemis-user-data-vars __TEST_VAR__ \
	--test-assets $(ROOT_DIR)/tests \
	--variables $(ROOT_DIR)/tests/.gluetool.d/variables.yaml --variables tests/common.yaml \
	--html report.html

##@ Utility

clean:  ## Cleanup virtualenv and tests data
	rm -rf $(VENV) assets report.html

# See https://www.thapaliya.com/en/writings/well-documented-makefiles/ for details.
reverse = $(if $(1),$(call reverse,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1))

help:  ## Show this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make [target]\033[36m\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(call reverse, $(MAKEFILE_LIST))
